ifndef jobs
jobs = 24
endif

ifdef debug
ifeq ($(debug),true)
.SECONDARY:
endif
endif

reference = data/genome/scaffolds.fa
filter_length_perc = 85
mafft_params = -p '--op 8'
revcomp = tools/scripts/checkered-revcomp |

lordec = $$HOME/Software/lordec/lordec
k_corr = 23
illumina_library = $(shell find data/illumina/*.fastq | tr '\n' ',' | sed "s/,$$//g")

minimap = $$HOME/Software/minimap/minimap
topmap_fraction = 1
max_contig_overlap = 0.05

all: data/annotations/all-trimmed-scafmap.dat

include include/tests.mk
include include/correction.mk
include include/conversion.mk
include include/orf-checker.mk
include include/consensus.mk
include include/mapping.mk
