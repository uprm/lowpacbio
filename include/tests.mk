data/analyses/%_fastqc.html: data/reads/%.fastq
	fastqc --outdir $$(dirname "$@") --noextract "$<"
	rm -f $$(dirname "$@")/*.zip

data/analyses/%.quast.txt: data/genome/%.fa
	quast -t $(jobs) -o temp/$$(basename "$@")_quast "$<"
	mv temp/$$(basename "$@")_quast/report.txt "$@"
	rmdir temp/$$(basename "$@")_quast
