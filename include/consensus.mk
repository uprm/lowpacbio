data/reads/%-consensus.fa: data/reads/%-grouped.fa
	(cat "$<" \
	| tools/scripts/filter-short $(filter_length_perc) \
	| tools/scripts/batch-subreads \
	| parallel --gnu --plain -j$(jobs) tools/scripts/consensus) \
	> "$@"
