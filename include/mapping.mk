data/alignments/%.map: data/reads/%-consensus.fa
	$(minimap) -t $(jobs) \
		"$(reference)" "$<" \
		| sort -k11,11nr -s \
		> "$@"

data/alignments/%.topmap: data/alignments/%.map
	tools/scripts/topmap "$<" $(topmap_fraction) > "$@"

data/alignments/%.scafmap: data/alignments/%.topmap
	cat "$<" | tools/scripts/find-scaffolders $(max_contig_overlap) > "$@"

data/annotations/%-scafmap.dat: data/reads/%.fa data/alignments/%.scafmap
	cat $(word 2, $^) \
		| tools/scripts/scaffolder-stats \
		$$(LC_ALL=C fgrep -ic --mmap '>' $<) \
		$$(LC_ALL=C fgrep -ic --mmap '>' $(reference)) \
		> "$@"
