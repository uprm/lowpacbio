data/annotations/%-indel-stats.dat: data/annotations/%-coverage-rank.dat data/reads/%-ranked.fa
	cat /dev/null > temp/$$(basename "$@" ".dat").fa
	rm -f "$@.png" || :
	cat "$<" | awk '{print $$2}' | sed "s/>//g" \
	| xargs -P$(jobs) -I% sh -c \
	"fgrep -A1 --no-group-separator '%' '$(word 2, $^)' \
	| $(norevcomp) dobby wrap.mafft $(mafft_params) 2>/dev/null \
	| dobby util.fasta.flatten \
	| tee -a temp/$$(basename $@ .dat).fa \
	| tools/scripts/indel-stats" \
	| tee "$@" \
	| tools/scripts/indel-histograms "$@.png" 2 \
	| 2>/dev/null || :
