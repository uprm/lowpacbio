data/reads/all.fa: | data/reads
	for fq in $$(find $|/*.fastq); do cat "$$fq"; done \
	| dobby util.fastq.2fa \
	> "$@"

data/reads/%-ranked.fa: data/reads/%.fa
	dobby util.fasta.flatten -i "$<" \
	| tools/scripts/filter-short $(filter_length_perc) \
	| dobby util.fasta.sort -p "length,reverse" \
	> "$@"

data/annotations/%-coverage-rank.dat: data/reads/%-ranked.fa
	cat "$<" \
	| grep -Fi ">" | cut -f1 -d" " | sed "s/[0-9_]\+$$//g" \
	| sort | uniq -c | sort -k1,1rn \
	| sed "s/^\s\+//g" \
	> "$@"

data/reads/%-grouped.fa: data/reads/%.fa
	LC_ALL=C cat "$<" \
	| dobby util.fasta.flatten \
	| tr '\n' '<' | tr '>' '\n' \
	| grep -v '^$$' | sed 's/^/>/g' | sed 's/<$$//g' \
	| sort -s \
	| tr '<' '\n' \
	> "$@"
