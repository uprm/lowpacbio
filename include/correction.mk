data/reads/%-corrected.fa: data/reads/%-ranked.fa
	$(lordec)-correct \
		-T $(jobs) \
		-k $(k_corr) \
		-s 3 \
		--long_reads "$<" \
		--short_reads $(illumina_library) \
		-o "$@"

data/reads/%-trimmed.fa: data/reads/%-corrected.fa
	$(lordec)-trim \
		-i "$<" \
		-o temp/$$(basename "$@")
	cat temp/$$(basename "$@") \
		| tools/scripts/lowercase2n \
		| dobby util.fasta.flatten \
		> "$@"
	rm -f temp/$$(basename "$@")
